Controlling a Keysight Oscilloscope via peary

The device automatically fetches data from the cannels x enabled via `:CHANnel<X>:DISPlay On`. If a single digital channel is active via `:DIGital<x>:DISPlay 1` all 16 digital channels are being read out. The converter fetches this information automatically and extracts the trigger ID from the digital channels via the fixed channel mapping On request we can also generalize this:

* digital 1 = trigger
* digital 5 = trigger ID
* digital 14 = clock

Since the TLU always uses 40 MHz to clock out the ID, the EUDAQ converter (add link once its in EUDAQ) automatically fixes the binwidth to 25ns and simply returns the ID. If the digital channels are used for triggerID storage, it is essential to not ignore the busy of the telescope as the TLU is not receiving a busy from the oscilloscope and hence it can happen to overlay two trigger IDs resulting in flawed conversion



## example Configuration

```
# reset settings
*RST

# setup channels
:CHANnel1:DISPlay On
:CHANnel2:DISPlay On
:CHANnel3:DISPlay On
:CHANnel4:DISPlay On
:CHANnel1:SCALe 1
:CHANnel2:SCALe 500E-3
:CHANnel3:SCALe 500E-3
:CHANnel4:SCALe 1
:CHANnel1:OFFSet 0
:CHANnel2:OFFSet 900E-3
:CHANnel3:OFFSet 900E-3
:CHANnel4:OFFSet 0

# Label Channels
:CHANnel1:LABel "TLU Signal"
:CHANnel2:LABel "TelePix Hitbus"
:CHANnel3:LABel "Injection Signal"
:CHANnel4:LABel "TLU ID"
:DISPlay:LABel 1

# Impedances DC ( DC 1 MO) DC50 (DC 50O)
:CHANnel1:INPut DC50
:CHANnel2:INPut DC
:CHANnel3:INPut DC
:CHANnel4:INPut DC50


# Skew (Use To adjust so minimum recording time needed) Range -1ms to 1ms
:CHANnel1:PROBe:SKEW 0
:CHANnel3:PROBe:SKEW -3000E-9
:CHANnel2:PROBe:SKEW -3000E-9
:CHANnel4:PROBe:SKEW 0

# setup time axis
:TIMebase:SCALe 100E-9
:TIMebase:POSition >250E-9


# waveform settings
:WAVeform:STReaming ON
:WAVeform:SEGMented:ALL ON
:WAVeform:BYTEorder LSBFirst
:WAVeform:FORMat WORD
:WAVeform:SOURce CHANnel1
#:WAVeform:SOURce CHANnel2
:WAVeform:SOURce CHANnel3
:WAVeform:SOURce CHANnel4


# Digital Waveforms
:DIGital0:DISPlay 1
:DIGital1:DISPlay 1
:DIGital2:DISPlay 1
:DIGital3:DISPlay 0
:DIGital4:DISPlay 1
:DIGital5:DISPlay 1
:DIGital6:DISPlay 1
:DIGital7:DISPlay 0
:DIGital8:DISPlay 1
:DIGital9:DISPlay 1
:DIGital10:DISPlay 1
:DIGital11:DISPlay 1
:DIGital12:DISPlay 1
:DIGital13:DISPlay 1
:DIGital14:DISPlay 1
:DIGital15:DISPlay 0



# To change to the vertical size of display DIGital<N>:SIZE {SMALl | MEDium | LARGe}
# To change the threshold :DIGital<N>:THReshold • CMOS50 = 2.5 V • CMOS33 = 1.65 V • CMOS25 = 1.25 V • ECL = -1.3 V • PECL = 3.7 V • TTL = 1.4 V • DIFFerential = 0 V
:DIgital1:LABel "TLU"
:DIGital1:SIZE LARGe
:DIGital1:THReshold 100E-3
:DIgital14:LABel "cssssslk"
:DIGital14:SIZE LARGe
:DIGital14:THReshold 100E-3
:DIgital5:LABel "Trigger id"
:DIGital5:SIZE LARGe
:DIGital5:THReshold 100E-3
#:ACQuire:SRATe:DIGital AUTO
#:WAVeform:SOURce POD1
#:WAVeform:SOURce POD2

# trigger setting
:TRIGger:MODE EDGE
:TRIGger:EDGE:SLOPe POSitive
#:TRIGger:LEVel CHANnel1, 530E-3
:TRIGger:EDGE:SOURce DIGital1
:TRIGger:SWEep TRIGgered

# acquisition mode
:ACQuire:MODE SEGMENTED
:ACQuire:INTerpolate 0
:ACQuire:SEGMented:COUNt 10
:ACQuire:POINts 8200
:ACQuire:SRATe:analog 10e9
```