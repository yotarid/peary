#ifndef CLICTD_FRAMEDECODER_HPP
#define CLICTD_FRAMEDECODER_HPP

#include <vector>

#include "peary/utils/datatypes.hpp"
#include "peary/utils/log.hpp"
#include "peary/utils/utils.hpp"

#include "CLICTDPixels.hpp"

#define CLICTD_FRAME_START 0b1111111111111110101000
#define CLICTD_FRAME_END 0b1111111111111110010100
#define CLICTD_COLUMN_ID 0b1111111111111101000000u
#define CLICTD_COLUMN_ID_MASK 0b0000000000000000111100u
#define CLICTD_COLUMN_ID_MASK_SHIFT 2
#define CLICTD_PIXEL_BITS 22
#define CLICTD_COLUMNS 16
#define CLICTD_ROWS 128

namespace caribou {
  class CLICTDFrameDecoder {
  public:
    CLICTDFrameDecoder(bool long_counter = false) : longcnt(long_counter){};

    void setLongCounter(bool value) { longcnt = value; };

    template <typename T> pearydata decodeFrame(const pearyRawData& rawFrame, bool decode_lfsr = true);

    template <typename T> std::vector<T> splitFrame(const pearyRawData& rawFrame);

  private:
    template <typename T> T getNextPixel(const pearyRawData& rawFrame, unsigned& word, unsigned& bit);
    bool longcnt{};
  };
} // namespace caribou

#endif
