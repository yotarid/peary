/**
 * Caribou implementation for the DESYER1
 */

#include "DESYER1Device.hpp"
#include "utils/log.hpp"

#include <fstream>

using namespace caribou;

DESYER1Device::DESYER1Device(const caribou::Configuration config)
    : CaribouDevice(config, iface_mem::configuration_type(MEM_PATH, DESYER1_MEM)) {
  // Register device-specific commands
  _dispatcher.add("chipReset", &DESYER1Device::chipReset, this);
  // Set up periphery
  _periphery.add("v_dummypix", carboard::BIAS_1); // (No10) 0.1-0.5V, 0. 35V nom.
  _periphery.add("v_krumref", carboard::BIAS_2);  // (No15) 0.325V nom., resolution < 2mV
  _periphery.add("v_compthr", carboard::BIAS_3);  // (No14) 0.325V nom., resolution < 2mV
  _periphery.add("i_sf", carboard::CUR_1);        // (No21) typ 95uA , resolution < 10uA
  _periphery.add("i_krum", carboard::CUR_2); // (No19) typ 2.1uA, resolution < 10nA - will most likely be supplied externally
  _periphery.add("i_csa", carboard::CUR_3); // (No17) typ 2.4uA, resolution < 100nA - will most likely be supplied externally
  _periphery.add("CMOS_OUT_1_TO_4", carboard::CMOS_OUT_1_TO_4);
  _periphery.add("CMOS_IN_1_TO_4", carboard::CMOS_IN_1_TO_4);

  // Add the register definitions to the dictionary for convenient lookup of names:
  _registers.add(DESYER1_REGISTERS);

  // Add memory pages to the dictionary:
  _memory.add(DESYER1_MEMORY);

  this->setMemory("fpga_rst", 1);

  /** Set up the slow control to use Zybc-board SMA ports
   *
   * Memory:
   * debug1 - usr_clk N
   * debug2 - usr_clk P
   *
   * Value:
   * 0 - cntrl_clk
   * 1 - cntrl_dat
   * 2 - cntrl_rst
   * 3 - sr_clk
   */
  this->setMemory("debug1", 1);
  this->setMemory("debug2", 2);
  mDelay(10);
}

/**
 * Configure the slow control of the DESYER1 device
 */
void DESYER1Device::configure() {
  LOG(INFO) << "Configuring DESYER1 device";
  /**
   * TODO: How does using the slow control work? do we just wait for some time in between configuring the individual pixels?
   * TODO: Does setting registers to 0b00 etc. work? ~Jona
   */
  LOG(DEBUG) << "resetting chip";
  this->setMemory("chip_rst", 1);
  usleep(200);

  for(int i_pix = 0; i_pix < 4; i_pix++) {
    std::string pix_mask = "pixel_mask_" + std::to_string(i_pix);
    std::string pix_krum = "pixel_krum_" + std::to_string(i_pix);
    std::string pix_compthr = "pixel_compthr_" + std::to_string(i_pix);

    if(_config.Has(pix_mask) && _config.Has(pix_krum) && _config.Has(pix_compthr)) {
      LOG(DEBUG) << "configuring pixel " << i_pix << " from config";
      configurePixel(i_pix, _config.Get(pix_mask, 1), _config.Get(pix_krum, 5.53), _config.Get(pix_compthr, 0.0));
    } else {
      LOG(DEBUG) << "No complete pixel configuration for pixel" << i_pix
                 << "found in config file. Configuring to default values.";
      configurePixel(i_pix, true, 5.53, 0.0);
    }
  }
}

/**
 * Configure a pixel,  input values are integer
 * @param pix_number can be 0:3 = 0b00, 0b01, 0b10, 0b11
 * @param pix_power 1 for on, 0 for off
 * @param krum_bias 2.32, 3.30, 4.57, 5.53, 6.78, 7.73, 8.96, 9.91
 * @param thresh_trimming 0, +-54.3, +-107.8, +-161.0, +-213.8, +-266.4, +-318.8, +-370.6
 */

void DESYER1Device::configurePixel(int pix_number, bool pix_power, double krum_bias, double thresh_trimming) {
  LOG(DEBUG) << "Configuring pixel " << pix_number << " with PWRdown=" << pix_power
             << " , krum_bias=" << krum_bias_map_val_b.at(krum_bias)
             << " , thresh_trimming=" << thresh_trimming_map_val_b.at(thresh_trimming);

  setRegister("pix_address", static_cast<long unsigned int>(pix_number));
  usleep(200);
  setRegister("pix_power", static_cast<long unsigned int>(pix_power));
  usleep(200);
  setRegister("krum_bias", static_cast<long unsigned int>(krum_bias_map_val_b.at(krum_bias)));
  usleep(200);
  setRegister("thresh_trimming", static_cast<long unsigned int>(thresh_trimming_map_val_b.at(thresh_trimming)));
  usleep(200);
  return;
}

void DESYER1Device::chipReset() {
  LOG(INFO) << "Resetting chip";
  this->setMemory("chip_rst", 1);
  return;
}

pearyRawData DESYER1Device::getRawData() {
  return {};
}

void DESYER1Device::powerUp() {
  LOG(DEBUG) << "We do in fact have the new code with correct current directions.";

  LOG(INFO) << "Powering up";

  LOG(DEBUG) << " CMOS_OUT_1_TO_4: " << _config.Get("cmos_out_1_to_4_voltage", DESYER1_CMOS_OUT_1_TO_4) << "V";
  this->setVoltage("CMOS_OUT_1_TO_4", _config.Get("cmos_out_1_to_4_voltage", DESYER1_CMOS_OUT_1_TO_4));
  this->switchOn("CMOS_OUT_1_TO_4");

  LOG(DEBUG) << " CMOS_IN_1_TO_4: " << _config.Get("cmos_in_1_to_4_voltage", DESYER1_CMOS_IN_1_TO_4) << "V";
  this->setVoltage("CMOS_IN_1_TO_4", _config.Get("cmos_in_1_to_4_voltage", DESYER1_CMOS_IN_1_TO_4));
  this->switchOn("CMOS_IN_1_TO_4");

  LOG(DEBUG) << "Setting voltages";

  this->switchOff("v_dummypix");
  this->switchOff("v_krumref");
  this->switchOff("v_compthr");
  this->switchOff("i_sf");
  this->switchOff("i_krum");
  this->switchOff("i_csa");

  LOG(INFO) << "Setting voltages";

  if(_config.Get("v_dummypix", DESYER1_DEFAULT_VDUMMYPIX) > 0.5 ||
     _config.Get("v_dummypix", DESYER1_DEFAULT_VDUMMYPIX) < 0.0) {
    LOG(WARNING) << "v_dummypix would be set to more than 0.5 V, this will fry your chip. setting to default"
                 << DESYER1_DEFAULT_VDUMMYPIX << " V";
    this->setVoltage("v_dummypix", DESYER1_DEFAULT_VDUMMYPIX, DESYER1_DEFAULT_VDUMMYPIX_CURRENT);
  } else {
    LOG(DEBUG) << " setting v_dummypix" << _config.Get("v_dummypix", DESYER1_DEFAULT_VDUMMYPIX) << "V";
    this->setVoltage("v_dummypix",
                     _config.Get("v_dummypix", DESYER1_DEFAULT_VDUMMYPIX),
                     _config.Get("v_dummypix_current", DESYER1_DEFAULT_VDUMMYPIX_CURRENT));
  }
  this->switchOn("v_dummypix");
  mDelay(100); // waiting time in ms,

  if(_config.Get("v_krumref", DESYER1_DEFAULT_VKRUMREF) > 0.5 || _config.Get("v_krumref", DESYER1_DEFAULT_VKRUMREF) < 0.0) {
    LOG(WARNING) << "v_krumref would be set to more than 0.5 V, this might fry your chip. setting to default: "
                 << DESYER1_DEFAULT_VKRUMREF << " V";
    this->setVoltage("v_krumref", DESYER1_DEFAULT_VKRUMREF, DESYER1_DEFAULT_VKRUMREF_CURRENT);
  } else {
    this->setVoltage("v_krumref",
                     _config.Get("v_krumref", DESYER1_DEFAULT_VKRUMREF),
                     _config.Get("v_krumref_current", DESYER1_DEFAULT_VKRUMREF_CURRENT));
    LOG(DEBUG) << " setting v_krumref" << _config.Get("v_krumref", DESYER1_DEFAULT_VKRUMREF) << "V";
  }
  this->switchOn("v_krumref");
  mDelay(100);

  if(_config.Get("v_compthr", DESYER1_DEFAULT_VCOMPTHRESH) > 0.5 ||
     _config.Get("v_compthr", DESYER1_DEFAULT_VCOMPTHRESH) < 0.0) {
    LOG(WARNING) << "v_compthr would be set to more than 0.5 V, this might fry your chip. setting to default: "
                 << DESYER1_DEFAULT_VCOMPTHRESH << " V";
    this->setVoltage("v_compthr", DESYER1_DEFAULT_VCOMPTHRESH, DESYER1_DEFAULT_VCOMPTHRESH_CURRENT);
  } else {
    LOG(DEBUG) << " setting v_compthr" << _config.Get("v_compthr", DESYER1_DEFAULT_VCOMPTHRESH) << "V";
    this->setVoltage("v_compthr",
                     _config.Get("v_compthr", DESYER1_DEFAULT_VCOMPTHRESH),
                     _config.Get("v_compthr_current", DESYER1_DEFAULT_VCOMPTHRESH_CURRENT));
  }
  this->switchOn("v_compthr");
  mDelay(100);

  LOG(INFO) << "Setting currents";
  // 1-push 0-pull

  if(_config.Get("i_sf", DESYER1_DEFAULT_I2C) > 120) {
    LOG(WARNING) << "i_sf would be set to more than 120 uA, this might fry your chip. setting to default: "
                 << DESYER1_DEFAULT_I2C << " uA";
    this->setCurrent("i_sf", DESYER1_DEFAULT_I2C, 1); // push
  } else {
    LOG(DEBUG) << " setting i_sf (source follower)" << _config.Get("i_sf", DESYER1_DEFAULT_I2C) << "A";
    this->setCurrent("i_sf", static_cast<unsigned int>(abs(_config.Get("i_sf", DESYER1_DEFAULT_I2C))), 1); // push
  }
  this->switchOn("i_sf");
  mDelay(100);

  if(_config.Has("i_csa")) {
    if(_config.Get("i_csa", DESYER1_DEFAULT_ICSA) > 5) {
      LOG(WARNING) << "i_csa would be set to more than 5 uA, this might fry your chip. setting to default: "
                   << DESYER1_DEFAULT_ICSA << " uA";
      this->setCurrent("i_csa", DESYER1_DEFAULT_ICSA, 1); // push
    } else {
      LOG(DEBUG) << " setting i_csa" << _config.Get("i_csa", DESYER1_DEFAULT_ICSA) << "A";
      this->setCurrent("i_csa", static_cast<unsigned int>(abs(_config.Get("i_csa", DESYER1_DEFAULT_ICSA))), 1); // push
    }
    this->switchOn("i_csa");
    mDelay(100);
  }

  if(_config.Has("i_krum")) {
    if(_config.Get("i_krum", DESYER1_DEFAULT_IKRUM) > 5) {
      LOG(WARNING) << "i_krum would be set to more than 5 uA, this might fry your chip. setting to default: "
                   << DESYER1_DEFAULT_IKRUM << " uA";
      this->setCurrent("i_krum", DESYER1_DEFAULT_IKRUM, 0); // pull
    } else {
      LOG(DEBUG) << " setting i_krum" << _config.Get("i_krum", DESYER1_DEFAULT_IKRUM) << "A";
      this->setCurrent("i_krum", static_cast<unsigned int>(abs(_config.Get("i_krum", DESYER1_DEFAULT_IKRUM))), 0); // pull
      this->switchOn("i_krum");
    }
  }
}

void DESYER1Device::powerDown() {
  LOG(INFO) << "Powering down voltages and currents.";

  LOG(DEBUG) << "\tPowering down v_dummypix";
  this->switchOff("v_dummypix");
  LOG(DEBUG) << "\tPowering down v_krumref";
  this->switchOff("v_krumref");
  LOG(DEBUG) << "\tPowering down v_compthr";
  this->switchOff("v_compthr");
  LOG(DEBUG) << "\tPowering down i_sf";
  this->switchOff("i_sf");
  LOG(DEBUG) << "\tPowering down i_krum";
  this->switchOff("i_krum");
  LOG(DEBUG) << "\tPowering down i_csa";
  this->switchOff("i_csa");
  LOG(DEBUG) << "\tPowering down CMOS_OUT_1_TO_4";
  this->switchOff("CMOS_OUT_1_TO_4");
  LOG(DEBUG) << "\tPowering down CMOS_IN_1_TO_4";
  this->switchOff("CMOS_IN_1_TO_4");
}

void DESYER1Device::daqStart() {
  LOG(INFO) << "DAQ started.";
}

void DESYER1Device::daqStop() {
  LOG(INFO) << "DAQ stopped.";
}

pearydata DESYER1Device::getData() {
  return {};
}
