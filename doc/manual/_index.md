---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Peary Software"
description: "The Caribou DAQ software framework"
weight: 5
---

This is the user manual for the Peary Caribou software, a DAQ framework for the Caribou DAQ system.
It aims to provide an overview of the software framework and facilitating the implementation of new devices.

## Introduction

The Peary DAQ software framework provides hardware abstraction for periphery components such as voltage regulators and simplifies direct detector configuration and data acquisition through a common interface.

A command line tool as well as a producer for EUDAQ2.0 integration is provided.

The software name derives from the smallest of the North American caribou subspecies, found in the high Arctic islands of Canada ([Wikipedia](https://en.wikipedia.org/wiki/Peary_caribou)).

### Source Code, Support and Reporting Issues
The source code is hosted on the CERN GitLab instance in the [peary repository](https://gitlab.cern.ch/Caribou/peary).
As for most of the software used within the high-energy particle physics community, only limited support on best-effort basis for this software can be offered.
The authors are, however, happy to receive feedback on potential improvements or problems arising.
Reports on issues, questions concerning the software as well as the documentation and suggestions for improvements are very much appreciated.
All issues and questions concerning this software should be reported on the projects [issue tracker](https://gitlab.cern.ch/Caribou/peary/issues), issues concerning the [Caribou Linux distribution](https://gitlab.cern.ch/Caribou/meta-caribou) or the [Peary firmware](https://gitlab.cern.ch/Caribou/peary-firmware) should be reported to the corresponding projects.

### Contributing Code
Peary is a community project that benefits from active participation in the development and code contributions from users.
Users and prospective developers are encouraged to discuss their needs either via the issue tracker of the repository to receive ideas and guidance on how to implement a specific feature.
Getting in touch with other developers early in the development cycle avoids spending time on features which already exist or are currently under development by other users.

## License

The Peary Caribou software is released under the __GNU Lesser General Public License v3.0__, a copy of the license text can be found [in the repository](https://gitlab.cern.ch/Caribou/peary/tree/master/LICENSE.md).
