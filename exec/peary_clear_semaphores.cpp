#include <iostream>

#include "peary/utils/log.hpp"
#include "peary/utils/utils.hpp"

using namespace caribou;

int main(int, char**) {
  // Add cout as the default logging stream
  Log::addStream(std::cout);
  Log::setReportingLevel(LogLevel::INFO);

  LOG(INFO) << "Clearing sempahores...";
  release_flock("/pearydev");
  release_flock("/pearydevmgr");
  LOG(INFO) << "done.";

  return 0;
}
