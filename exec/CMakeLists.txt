FIND_PACKAGE(Readline REQUIRED)
FIND_PACKAGE(Curses REQUIRED)

ADD_EXECUTABLE(pearycli "commandline/pearycli.cpp" "commandline/clicommands.cpp" "commandline/Console.cpp")
TARGET_LINK_LIBRARIES(pearycli ${PROJECT_NAME} ${Readline_LIBRARY} ${CURSES_LIBRARIES})

ADD_EXECUTABLE(pearyd pearyd.cpp)
TARGET_LINK_LIBRARIES(pearyd ${PROJECT_NAME})

ADD_EXECUTABLE(peary_clear_semaphores peary_clear_semaphores.cpp)
TARGET_LINK_LIBRARIES(peary_clear_semaphores ${PROJECT_NAME})

INSTALL(TARGETS pearycli pearyd peary_clear_semaphores
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})
