/* This file contains the configuration class used by the Caribou libraries
 */

#ifndef CARIBOU_CONFIG_H
#define CARIBOU_CONFIG_H

#include <iomanip>
#include <map>
#include <string>
#include <vector>

#include "peary/utils/utils.hpp"

namespace caribou {

  class Configuration;

  /**
   * @class ConfigParser
   * @brief Simple parser class for TOML-like configurations consisting of sections indicated by [...] and key-value pairs.
   * Each section is stored in a separate Configuration object and can be obtained via its section name.
   */
  class ConfigParser {
  public:
    /** Default constructor */
    ConfigParser() = default;

    /**
     * @brief Parse from a configuration file via its path
     *
     * @param path Path to the configuration file
     */
    explicit ConfigParser(const std::string& path);

    /**
     * @brief Parse configuration from a file stream
     *
     * @param conffile File stream to read configuration from
     */
    explicit ConfigParser(std::istream& conffile);

    /**
     * @brief Check if this parser has a specific configuration
     *
     * @param section Section to look for
     * @return True if section exists, false otherwise
     */
    bool Has(const std::string& section) const;

    /**
     * @brief Obtain a configuration identified by the section name
     *
     * @param section Section name to look for in the configuration file
     * @return Configuration object for this section
     * @throws ConfigInvalidKey if no such section was found
     */
    Configuration GetConfig(const std::string& section) const;

    /**
     * @brief Obtain the main configuration section, which is identified by an empty section name
     * If no main configuration was found, an empty Configuration object is returned
     *
     * @return Configuration object for the main configuration
     */
    Configuration GetMainConfig() const;

    /**
     * @brief Obtain all parsed configuration objects
     * @return Map with all configurations and their section names as keys
     */
    std::map<std::string, Configuration> GetAllConfigs() const { return configs_; };

    /**
     * @brief Store current configurations into a file
     *
     * @param file Target file stream to write to
     */
    void Save(std::ostream& file) const;

    /**
     * @brief Get all available section names
     * @return Vector of section names
     */
    std::vector<std::string> GetSections() const;

  private:
    void Load(std::istream& file);
    std::map<std::string, Configuration> configs_;
  };

  class Configuration {
  public:
    /** Default constructor */
    Configuration() = default;
    Configuration(const Configuration& other);

    /**
     * @brief Add new key-value pair to config. Already existing values with the same key will be overwritten.
     *
     * \param key Kay of the new parameter
     * \param value Value of the new parameter
     */
    void Add(const std::string& key, const std::string& value);

    std::string operator[](const std::string& key) const { return GetString(key); }
    bool Has(const std::string& key) const;
    std::string Get(const std::string& key, const std::string& def) const;
    double Get(const std::string& key, double def) const;
    int64_t Get(const std::string& key, int64_t def) const;
    uint64_t Get(const std::string& key, uint64_t def) const;
    template <typename T> T Get(const std::string& key) const { return caribou::from_string<T>(GetString(key)); }
    template <typename T> T Get(const std::string& key, T def) const {
      return caribou::from_string<T>(Get(key, caribou::to_string(def)));
    }
    template <typename T> std::vector<T> Get(const std::string& key, std::vector<T> def) const {
      return split(Get(key, std::string()), def, ',');
    }
    int Get(const std::string& key, int def) const;
    template <typename T> T Get(const std::string& key, const std::string fallback, const T& def) const {
      return Get(key, Get(fallback, def));
    }
    std::string Get(const std::string& key, const char* def) const {
      std::string ret(Get(key, std::string(def)));
      return ret;
    }
    std::string Get(const std::string& key, const std::string& fallback, const std::string& def) const {
      return Get(key, Get(fallback, def));
    }

    template <typename T> void Set(const std::string& key, const T& val);
    bool empty() const { return dictionary_.empty(); }
    std::string Name() const;
    Configuration& operator=(const Configuration& other);
    void Print(std::ostream& out) const;
    void Print() const;

  private:
    friend std::ostream& operator<<(std::ostream& os, const Configuration& c);
    std::string GetString(const std::string& key) const;
    void SetString(const std::string& key, const std::string& val);

    std::map<std::string, std::string> dictionary_;
  };

  inline std::ostream& operator<<(std::ostream& os, const Configuration& c) {
    for(const auto& j : c.dictionary_) {
      os << j.first << " = " << j.second << "\n";
    }
    return os;
  }

  inline std::ostream& operator<<(std::ostream& os, const ConfigParser& c) {
    c.Save(os);
    return os;
  }

  template <typename T> inline void Configuration::Set(const std::string& key, const T& val) {
    SetString(key, caribou::to_string(val));
  }

} // namespace caribou

#endif /* CARIBOU_CONFIG_H */
