#include "configuration.hpp"
#include "exceptions.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace caribou;

ConfigParser::ConfigParser(const std::string& path) {
  std::istringstream confstr(path);
  Load(confstr);
}

ConfigParser::ConfigParser(std::istream& conffile) {
  Load(conffile);
}

void ConfigParser::Save(std::ostream& stream) const {
  for(const auto& [key, config] : configs_) {
    if(!key.empty()) {
      stream << "[" << key << "]\n";
    }

    stream << config;
    stream << "\n";
  }
}

void ConfigParser::Load(std::istream& stream) {
  std::map<std::string, Configuration> configs;

  std::string current_section;
  for(;;) {
    std::string line;
    if(stream.eof()) {
      break;
    }
    std::getline(stream, line);
    size_t equals = line.find('=');
    if(equals == std::string::npos) {
      line = trim(line);
      if(line.empty() || line[0] == ';' || line[0] == '#') {
        continue;
      }
      if(line[0] == '[' && line[line.length() - 1] == ']') {
        line = std::string(line, 1, line.length() - 2);
        // TODO: check name is alphanumeric?
        current_section = line;
        // Add dictionary
        configs[current_section] = Configuration();
      }
    } else {
      std::string key = trim(std::string(line, 0, equals));
      std::transform(key.begin(), key.end(), key.begin(), ::tolower);
      // TODO: check key does not already exist
      // handle lines like: blah = "foo said ""bar""; ok." # not "baz"
      line = trim(std::string(line, equals + 1));
      if((line[0] == '\'' && line[line.length() - 1] == '\'') || (line[0] == '\"' && line[line.length() - 1] == '\"')) {
        line = std::string(line, 1, line.length() - 2);
      } else {
        size_t i = line.find_first_of(";#");
        if(i != std::string::npos) {
          line = trim(std::string(line, 0, i));
        }
      }
      configs[current_section].Add(key, line);
    }
  }
  configs_ = configs;
}

std::vector<std::string> ConfigParser::GetSections() const {
  std::vector<std::string> sections;
  for(const auto& sec : configs_) {
    if(!sec.first.empty()) {
      sections.push_back(sec.first);
    }
  }
  return sections;
}

bool ConfigParser::Has(const std::string& section) const {
  return configs_.find(section) != configs_.end();
}

Configuration ConfigParser::GetConfig(const std::string& section) const {
  const auto& it = configs_.find(section);
  if(it != configs_.end()) {
    return it->second;
  }

  throw ConfigInvalidKey("No configuration with section name " + section + " found");
}

Configuration ConfigParser::GetMainConfig() const {
  return GetConfig("");
}

Configuration::Configuration(const Configuration& other) : dictionary_(other.dictionary_) {}

Configuration& Configuration::operator=(const Configuration& other) {
  dictionary_ = other.dictionary_;
  return *this;
}

void Configuration::Add(const std::string& key, const std::string& value) {
  dictionary_[key] = value;
}

bool Configuration::Has(const std::string& key) const {
  return dictionary_.find(key) != dictionary_.end();
}

std::string Configuration::Get(const std::string& key, const std::string& def) const {
  try {
    return GetString(key);
  } catch(const std::exception&) {
    // ignore: return default
  }
  return def;
}

double Configuration::Get(const std::string& key, double def) const {
  try {
    return from_string<double>(GetString(key));
  } catch(const std::exception&) {
    // ignore: return default
  }
  return def;
}

int64_t Configuration::Get(const std::string& key, int64_t def) const {
  try {
    std::string s = GetString(key);
    return std::strtoll(s.c_str(), nullptr, 0);
  } catch(const std::exception&) {
    // ignore: return default
  }
  return def;
}
uint64_t Configuration::Get(const std::string& key, uint64_t def) const {
  try {
    std::string s = GetString(key);
    return std::strtoull(s.c_str(), nullptr, 0);
  } catch(const std::exception&) {
    // ignore: return default
  }
  return def;
}

int Configuration::Get(const std::string& key, int def) const {
  try {
    std::string s = GetString(key);
    return static_cast<int>(std::strtol(s.c_str(), nullptr, 0));
  } catch(const std::exception&) {
    // ignore: return default
  }
  return def;
}

void Configuration::Print(std::ostream& out) const {
  for(const auto& it : dictionary_) {
    out << it.first << " : " << it.second << std::endl;
  }
}

void Configuration::Print() const {
  Print(std::cout);
}

std::string Configuration::GetString(const std::string& key) const {
  auto i = dictionary_.find(key);
  if(i != dictionary_.end()) {
    return i->second;
  }
  throw caribou::ConfigMissingKey("Key \"" + key + "\" not found");
}

void Configuration::SetString(const std::string& key, const std::string& val) {
  dictionary_[key] = val;
}
