# Peary Caribou

DAQ framework for the Caribou DAQ System

## User manual

The user manual for the Peary DAQ software can be found on the [Caribou website](https://caribou-project.docs.cern.ch/docs/04_peary_software/)

## How to compile / install?

The compilation and installation procedure for Peary is described in the [manual](https://caribou-project.docs.cern.ch/docs/04_peary_software/01_installation/)
